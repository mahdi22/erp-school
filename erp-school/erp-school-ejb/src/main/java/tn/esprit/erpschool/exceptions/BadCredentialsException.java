package tn.esprit.erpschool.exceptions;

import javax.ejb.ApplicationException;

@ApplicationException
public class BadCredentialsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
