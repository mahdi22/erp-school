package tn.esprit.erpschool.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.erpschool.entites.Module;
import tn.esprit.erpschool.entites.ModulePK;
import tn.esprit.erpschool.exceptions.MoreThanOneResultException;
import tn.esprit.erpschool.exceptions.NoResultFoundException;
import tn.esprit.erpschool.services.contracts.IModuleCrudLocal;
import tn.esprit.erpschool.services.contracts.IModuleCrudRemote;

@Stateless
public class ModuleCrud implements IModuleCrudRemote, IModuleCrudLocal {

	@PersistenceContext
	EntityManager em;

	@Override
	public void addModule(Module module) {
		em.persist(module);
	}

	@Override
	public void updateModule(Module module) {
		em.merge(module);
	}

	@Override
	public void deleteModule(ModulePK id) throws NoResultFoundException {
		em.remove(findModuleById(id));
	}

	@Override
	public Module findModuleById(ModulePK id) throws NoResultFoundException {
		try {
			return em.find(Module.class, id);
		} catch (NoResultException e) {
			throw new NoResultFoundException();
		}
	}

	@Override
	public List<Module> findAllModules() {
		TypedQuery<Module> query = em.createNamedQuery("findAllModules",
				Module.class);
		return query.getResultList();
	}

	@Override
	public Module findModuleByLabel(String label)
			throws NoResultFoundException, MoreThanOneResultException {
		TypedQuery<Module> query = em.createNamedQuery("findModuleByLabel",
				Module.class);
		query.setParameter("label", label);
		try {
			return query.getSingleResult();
		} catch (NonUniqueResultException e) {
			throw new MoreThanOneResultException();
		} catch (NoResultException e) {
			throw new NoResultFoundException();
		}
	}

	@Override
	public List<Module> findAllModulesByEctsNumber(int ectsNumber) {
		TypedQuery<Module> query = em.createNamedQuery(
				"findModuleByEctsNumber", Module.class);
		return query.getResultList();
	}

	@Override
	public boolean isModuleLabelUnique(String label) {
		try {
			findModuleByLabel(label);
		} catch (NoResultFoundException e) {
			return true;
		} catch (MoreThanOneResultException e) {
			return false;
		}
		return false;

	}

}
