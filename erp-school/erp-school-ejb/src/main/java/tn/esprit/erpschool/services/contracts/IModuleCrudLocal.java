package tn.esprit.erpschool.services.contracts;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.erpschool.entites.Module;
import tn.esprit.erpschool.entites.ModulePK;
import tn.esprit.erpschool.exceptions.MoreThanOneResultException;
import tn.esprit.erpschool.exceptions.NoResultFoundException;

@Local
public interface IModuleCrudLocal {

	public void updateModule(Module module);

	public void deleteModule(ModulePK id) throws NoResultFoundException;

	public void addModule(Module module);

	public Module findModuleById(ModulePK id) throws NoResultFoundException;

	public Module findModuleByLabel(String label) throws NoResultFoundException, MoreThanOneResultException;

	public List<Module> findAllModulesByEctsNumber(int ectsNumber);

	public boolean isModuleLabelUnique(String label);

	public List<Module> findAllModules();

}
